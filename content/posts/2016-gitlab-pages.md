Title: Pelican on GitLab Pages!
Slug: pelican-on-gitlab-pages
Date: 2016-03-25
Modified: 2019-07-03 15:24:40
Tags: pelican, gitlab

This site is hosted on GitLab Pages!

The source code of this site is at <https://gitlab.com/pages/pelican>.

Learn about GitLab Pages at <https://pages.gitlab.io>.

# Test edit!
