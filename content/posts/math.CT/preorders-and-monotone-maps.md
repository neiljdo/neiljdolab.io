Title: Generative effects, Part 1 - Preorders and monotone maps
Date: 2019-07-16
Category: Category Theory
Tags: math, category theory, math.CT
Status: draft

## What are generative effects?

Fong and Spivak, in Chapter 1 of their [book][1], introduce the concept of **generative effects**, but before we can define it we need a little bit of setup.

Suppose we are trying to determine if a certain piece of code, a *system*, will compile or not, the *observation* we're interested in. Now, everything's easy if we consider different systems in isolation. Most of the time, though, we do *operations* between systems - in our example, we could be concatenating several code blocks and so on. *Generative effects* will show up whenever an operation is not *observation-preserving*. Knowing if such effects might occur is incredibly important, e.g. does concatenating compiling pieces of code result in a bigger piece of code that compiles as well?

Having defined, albeit loosely, generative effects, we now proceed to defining new concepts that could help make our study of such effects more mathematically precise (we'll return to generative effects after a few more posts, I promise!).

## What are preorders?

Given a set $X$, one can define a means of deciding whether a element $x$ of the set is "smaller" than another element $y$ - we write it down as $x ≤ y$. We only require that our notion of "smaller" obeys the following rules:

1.  $x ≤ x$ (*reflexivity*); and,
2.  if $x ≤ y$ and $y ≤ z$, then $x ≤ z$ (*transitivity*).

Collectively, we call the pair $(X, ≤)$ a **preorder**. We can visualize preorders, like so:

![Example of a preorder](/images/preorder-example.png)

In the diagram above, we have $X := \{\text{a, b, c, d, e}\}$ and we have an arrow from $x$ to $y$ if and only if $x ≤ y$. The purple arrows correspond to Rule 1 above while the orange arrow corresponds to Rule 2 (these arrows typically are not drawn to avoid clutter).

Here are a few more examples of preorders, together with their diagrams:

*   Discrete preorder - a preorder where the only relations are of the form $x ≤ x$
    ![Discrete preorder](/images/discrete-preorder.png)

*   Boolean preorder - a rather simple preorder on the set of Booleans $\mathbb{B} ≔ \{\texttt{true}, \texttt{false}\}$
    ![Boolean preorder](/images/booleans.png)

*   Natural numbers, $\mathbb{N}$
    ![Natural numbers](/images/natural-numbers.png)

*   Going back to the source code example above, we can define $X ≔ \{\text{set of all possible source code/code snippets}\}$. We can then define $x ≤ y$ if and only if the code snippet $x$ is used in $y$.

If $x ≤ y$ and $y ≤ x$, we could write $x ≅ y$ and say that the two are *equivalent*. Notice we didn't say that $x$ and $y$ are equal - if we're working with the all-familiar real numbers and $≤$, then they would be. In fact, if our preorder follows the following additional rule,

3.  $x ≅ y$ implies $x = y$ (*skeletality*);

then our preorder is a **partial order** or **poset**.

More interesting than a partial order is a preorder where **each and every** pair of elements $x, y ∈ X$, i.e. $∀x, y ∈ X$, either $x ≤ y$ or $y ≤ x$. Such a preorder is called a **total order**. The first preorder above (shown with colored arrows) is *NOT* a total order since we don't know how $a$ and $d$ are related. On the other hand, the natural numbers, $\mathbb{N}$, is also a total order. And yes, your right in thinking that the real numbers, $\mathbb{R}$, is also a total order.



## What are monotone maps?


[1]:  https://arxiv.org/abs/1803.05316
