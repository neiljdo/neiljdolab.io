Title: Let's learn applied category theory
Date: 2019-07-11 00:42:00
Category: Category Theory
Tags: math, category theory, math.CT
Status: published


## Motivation

A few months back, I started my self-learning journey with the [MIT 18.S097 Applied Category Theory course][1]. Initially, I was aiming to learn functional programming to improve how I structure and write code, in general, how I approach a problem and find a solution for it. However, after a couple of Youtube channel subscriptions and watched introductory videos, Youtube's recommendation engine suggested that I watch [videos on category theory][2] instead. I got intrigued by the topic, did a few more (precautionary) googling, and, eventually, jumped into the rabbit hole of category theory.


## Resources

Again, I am currently using the following resources in my self-study:

+   [MIT 18.S097 Applied Category Theory][1] - the MIT OCW Course taught by David Spivak and Brendan Fong,
+   [An Invitation to Applied Category Theory: Seven Sketches in Compositionality. ISBN: 9781108482295][3] - the book written by the same guys, available online for free on [arXiv][4], and
+   [MIT 18.S097 IAP 2019 lectures][5] - the complete set of lecture videos that I compiled into a playlist.

Of course, there are a lot of other excellent resources out there - I will link to those as I go along. Also, I'll be sticking with these resources for the time being to avoid clutter.


## What's Next?

I'm actually done with Chapter 1 of the book, but I decided to go write about what I learned from that chapter retrospectively. I'll (re)dive right into Chapter 1 in my next post, see you there!


[1]: https://ocw.mit.edu/courses/mathematics/18-s097-applied-category-theory-january-iap-2019/
[2]: https://www.youtube.com/watch?v=I8LbkfSSR58&list=PLbgaMIhjbmEnaH_LTkxLI7FMa2HsnawM_
[3]: http://www.amazon.com/exec/obidos/ASIN/1108482295/ref=nosim/mitopencourse-20
[4]: https://arxiv.org/abs/1803.05316
[5]: https://www.youtube.com/watch?v=UusLtx9fIjs&list=PLv4TinHrXAzt1tpJxaHOiTa_m7z-S6yMy
