Title: About
Slug: about
Date: 2019-07-03 03:08:05
Modified: 2019-07-11 20:03:00
Tags: about
Status: published


Hello there!

Welcome to my site where I post about stuff that I'm currently working on/grokking, with the hope that readers, including myself, would get a thing or two from my posts. My posts will usually revolve around math, physics, artificial intelligence, and maybe a bit of language learning (I'm learning French and Japanese!) here and there.

I'm no expert on any of those fields though! This is just me trying to organize my thoughts and making life easier for my future self.

Thank you for sticking around!

Neil John D. Ortega
