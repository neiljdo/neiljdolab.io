#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals


AUTHOR = 'Neil John D. Ortega'
SITENAME = 'neiljdo\'s Blog'
SITEURL = ''

PATH = 'content'
ARTICLE_PATHS = ["posts"]
ARTICLE_SAVE_AS = "{date:%Y}/{date:%m}/{date:%d}/{slug}.html"
ARTICLE_URL = "{date:%Y}/{date:%m}/{date:%d}/{slug}.html"
OUTPUT_PATH = 'public'

TIMEZONE = 'Asia/Manila'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),
#          ('Python.org', 'http://python.org/'),
#          ('Jinja2', 'http://jinja.pocoo.org/'),
#          ('You can modify those links in your config file', '#'),)

# Social widget
TWITTER_URL = "https://twitter.com/neiljdo"
GITHUB_URL = "https://github.com/neiljdo"
SOCIAL = (
    ("rss", "//neiljdo.gitlab.io/fees/all.atom.xml"),
    ("twitter", TWITTER_URL),
    ("github", GITHUB_URL),
)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

MARKUP = ('md', 'ipynb')
PLUGIN_PATHS = ['./plugins']
PLUGINS = ['ipynb.markup', 'render_math']

# If you create jupyter files in the content dir, snapshots are saved with the same
# metadata. These need to be ignored.
IGNORE_FILES = [".ipynb_checkpoints"]

DEFAULT_METADATA = {
    "status": "draft",
}

THEME = "pelican-themes/Flex"

# Flex settings
SITETITLE = "neiljdo"
COPYRIGHT_NAME = AUTHOR
COPYRIGHT_YEAR = 2019
MAIN_MENU = True
MENUITEMS = (
    ('Archives', '/archives.html'),
    ('Categories', '/categories.html'),
    ('Tags', '/tags.html'),
)
STATIC_PATHS = ["images", "stylesheet", ]
EXTRA_PATH_METADATA = {
    "images/profile.png": {"path": "theme/img/profile.png"},
    "stylesheet/custom.css": {"path": "static/custom.css"},
}
CUSTOM_CSS = "static/custom.css"


# clean-blog settings
